package com.yishuifengxiao.common.oauth2.provider;

import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

/**
 * token自动续签策略工具抽象类
 * 
 * @author yishui
 * @version 1.0.0
 * @since 1.0.0
 */
public abstract class TokenStrategy implements ResourceServerTokenServices {

}
